---
title: NFS server maintainence
categories: linux, nfs, raid, sa
...

## NTP

The NFS server must sync their system time with a reliable NTP
server.  Furthermore, NFS servere and client **must use the same
NTP server**.  Here's an example `/etc/systemd/timesyncd.conf`:

```
[Time]
NTP=tick.stdtime.gov.tw tock.stdtime.gov.tw time.stdtime.gov.tw clock.stdtime.gov.tw watch.stdtime.gov.tw
```

Don't forget to do:

``` bash
sudo timedatectl set-ntp true
sudo systemctl restart systemd-timesyncd
```

## Disk Array

### IBM DS 3500
This is the disk array by which our home directory file server current is attached.

* [Storage Manager](http://www.csie.ntu.edu.tw/~yunchih/stuff/IBM_DS_3500_manager_software.tar.xz)
* [Operation Manual](http://www.csie.ntu.edu.tw/~yunchih/stuff/https://www.csie.ntu.edu.tw/~yunchih/stuff/IBM_DS3500_Manual.pdf)
* [Command Line Interface Manual](http://www.csie.ntu.edu.tw/~yunchih/stuff/IBM_DS3500_Command_Line_Interface_and_Script_Comman.pdf)

Simple checking:

``` bash
sudo SMcli 192.168.128.105 -c "show allLogicalDrives summary;"
sudo SMcli 192.168.128.105 -c "show storagesubsystem;" 
```

where `192.168.128.105` is the IP of cspraid.

To do more sophisticated configuration, please use the graphical interface `SMClient`.  Setup ssh X11 forwarding before launching it.

#### First time setup
1. Wire up between host and raid with Ethernet.  Manually assign the host interface with a `192.168.128.0/24` IP.  The default IP of the controller is `192.168.128.101` (or try `192.168.128.102`), ping the IP to verify connectivity.
2. Once connected, use `SMcli -A` to auto discover disk array controller or `SMcli -A 192.168.128.101`.
3. Now, launch `SMclient`. (Step 2 can also be accomplish in `SMclient`)
4. Create disk volume
5. Configure host access
6. Create Host-channel mapping (beware ID conflict, see the Infotrend section below)

### Infotrend ES A12U-G2421

Use the LED panel for configuration.  When configuring LUN mapping, make sure the IDs do not conflict with host internal disk IDs.  All ID must be unique.  These IDs are typically shown in BIOS, if the hardware is correctly detected.  If conflict happens, the disk array will likely be neglected by the BIOS and thus invisible to host operating system.

## Trouble-shooting

### NFS cannot be un-mounted
1. Try `stat /nfs/undergrad` to see if the mountpoint can be mounted by `autofs` automatically.
2. If stuck, leave it as it is and do `umount -f /nfs/undergrad`.  Typically, `autofs` will automatically re-mount it.
3. If still fail, do `umount -l /nfs/undergrad` or `umount -lf /nfs/undergrad`.

### NFSv4 client stuck at `nfs4err_expired`
1. Do `mount nfs-server:/e/os/linux /nfs/linux -s -o rw,nosuid,nodev,tcp,hard,intr,nfsvers=4,rsize=8192,wsize=8192`.  If it stucks, leave it as it is.
2. On NFS server, do `exportfs -u nfs-client:/e/os`, then `exportfs -ar`.
3. Wait for a few seconds to see the `mount` command succeeds.
